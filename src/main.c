#include <stdio.h>

#include "mem.h"

#define REGION_SIZE 4096
#define debug_heap_err(...) debug_heap(stderr, __VA_ARGS__)
#define ONE_BYTE_SIZE (sizeof(uint8_t))

void debug(const char *fmt, ...);

void run_test(void (*test_func)(void *), const char* test_name) {
    void * heap = heap_init(0);
    debug(test_name);
    debug("... ");
    test_func(heap);
    debug_heap_err(heap);
    heap_term();
    debug("OK\n");
}

void test1_access_allocate_memory(void *main_heap) {
	void *block1 = _malloc(ONE_BYTE_SIZE);
	void *block2 = _malloc(ONE_BYTE_SIZE * 800);
	void *block3 = _malloc(ONE_BYTE_SIZE);
	debug_heap_err(main_heap);
	_free(block1);
	debug_heap_err(main_heap);
	_free(block2);
	debug_heap_err(main_heap);
	_free(block3);
}

void test_free_one_block(void *main_heap) {
	_malloc(ONE_BYTE_SIZE * 200);
	void *block = _malloc(ONE_BYTE_SIZE * 400);
	_malloc(ONE_BYTE_SIZE * 500);
	debug_heap_err(main_heap);
	_free(block);
}

void test_free_two_blocks_v1(void *main_heap1) {
	_malloc(ONE_BYTE_SIZE * 100);
	_malloc(ONE_BYTE_SIZE * 400);
	void *block1 = _malloc(ONE_BYTE_SIZE * 300);
	void *block2 = _malloc(ONE_BYTE_SIZE * 500);
	_malloc(ONE_BYTE_SIZE);
	debug_heap_err(main_heap1);
	_free(block1);
	debug_heap_err(main_heap1);
	_free(block2);
}

void test_free_two_blocks_v2(void *main_heap2) {
	_malloc(ONE_BYTE_SIZE * 800);
	void *block3 = _malloc(ONE_BYTE_SIZE * 200);
	_malloc(ONE_BYTE_SIZE * 400);
	void *block4 = _malloc(ONE_BYTE_SIZE * 600);
	_malloc(ONE_BYTE_SIZE * 100);
	debug_heap_err(main_heap2);
	_free(block3);
	debug_heap_err(main_heap2);
	_free(block4);
}

void test_grow_new_region(void *main_heap) {
	void *block1 = _malloc(REGION_SIZE);
	debug_heap_err(main_heap);
	void *block2 = _malloc(REGION_SIZE*2+1);
	debug_heap_err(main_heap);
	void *block3 = _malloc(REGION_SIZE);
	_free(block1);
	debug_heap_err(main_heap);
	_free(block2);
	debug_heap_err(main_heap);
	_free(block3);
}

void test_new_region_grow_in_another_place(void *main_heap) {
	void *blocked_block_addr = main_heap + REGION_SIZE;
	void* blocked_block = mmap(blocked_block_addr, REGION_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
	void *block1 = _malloc(REGION_SIZE*2+ONE_BYTE_SIZE*32);
	debug_heap_err(main_heap);
	void *block2 = _malloc(REGION_SIZE*1+ONE_BYTE_SIZE*32);
	debug_heap_err(main_heap);
	_free(block1);
	debug_heap_err(main_heap);
	_free(block2);
	debug_heap_err(main_heap);
	_free(blocked_block);
}

int main() {
    run_test(test1_access_allocate_memory, "Test 1: access allocate memory");
    run_test(test_free_one_block, "Test 2: Free one block");
    run_test(test_free_two_blocks_v1, "Test 3: Free two blocks");
    run_test(test_free_two_blocks_v2, "Test 4: Free two blocks");
    run_test(test_grow_new_region, "Test 5: Grow new region");
    run_test(test_new_region_grow_in_another_place, "Test 6: Test new region grow in another place");
	return 0;
}
