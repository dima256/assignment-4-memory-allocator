#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );


extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd);
static bool            block_is_big_enough( size_t query_capacity, struct block_header* block ) { return block->capacity.bytes >= query_capacity; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}
#define BLOCK_MIN_CAPACITY 24

static size_t capacity_max( size_t query_capacity ) { return size_max( query_capacity , BLOCK_MIN_CAPACITY); }
static size_t region_actual_size( size_t query_size ) { return size_max( round_pages( query_size ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query_capacity ) {
	bool extends = true;
	size_t query_size = region_actual_size(query_capacity + offsetof( struct block_header, contents ));
	void* new_addr = map_pages(addr, query_size, MAP_FIXED);
	if(new_addr == MAP_FAILED) {
		new_addr = map_pages(addr, query_size, 0);
		if(new_addr == MAP_FAILED) return (struct region) {0};
		extends = false;
	}
	block_init(new_addr, (block_size) {query_size}, NULL);
	return (struct region) {new_addr, query_size, extends};
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
	struct block_header *block_pointer = (struct block_header *) HEAP_START;
	while (block_pointer) {
		struct block_header *next_block_pointer = block_pointer->next;
		block_size to_free = size_from_capacity(block_pointer->capacity);
		while (next_block_pointer && blocks_continuous(block_pointer, next_block_pointer)) {
			to_free.bytes += size_from_capacity(next_block_pointer->capacity).bytes;
			next_block_pointer = next_block_pointer->next;
		}
		if (munmap(block_pointer, to_free.bytes)==-1) {
			printf("Error: munmap failed.\n");
		}
		block_pointer = next_block_pointer;
	}
}


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query_capacity) {
  return block-> is_free && query_capacity + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query_capacity ) {
	if(block_splittable(block, query_capacity)) {
		void* second_part_block = (block->contents + query_capacity);
		block_init(second_part_block, (block_size){block->capacity.bytes - query_capacity}, block->next);
		block->next = second_part_block;
		block->capacity.bytes = query_capacity;
		return true;
	}
	return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
	if (!block) return false;
	struct block_header* next_block = block->next;
	if (!next_block) return false;
	if (mergeable(block, next_block)) {
		block_init(block, (block_size){.bytes = size_from_capacity(block->capacity).bytes + size_from_capacity(next_block->capacity).bytes}, next_block->next);
		return true;
	}
	return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last( struct block_header* restrict block, size_t query_capacity ) {
	if (!block) return (struct block_search_result) {.type = BSR_CORRUPTED};
	struct block_header *previous_block_pointer = NULL;
	while (block) {
		while (try_merge_with_next(block))
			;
		if (block->is_free && block_is_big_enough(query_capacity, block)) {
			return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
		}
		previous_block_pointer = block;
		block = block->next;
	}
	return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = previous_block_pointer};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query_capacity, struct block_header* block ) {
	if (!block) return (struct block_search_result) {.type = BSR_CORRUPTED};
	struct block_search_result find_result = find_good_or_last(block, query_capacity);
	if (find_result.type == BSR_FOUND_GOOD_BLOCK) {
		split_if_too_big(find_result.block, query_capacity);
		find_result.block->is_free = false;
		return find_result;
	}
	return find_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query_capacity ) {
	if (!last) return NULL;
	struct region new_region = alloc_region(block_after(last), capacity_max(query_capacity));
	if (region_is_invalid(&new_region)) return NULL;
	block_init(new_region.addr, (block_size){.bytes = new_region.size}, NULL);
	last->next = new_region.addr;
	if (last->is_free && try_merge_with_next(last)) return last;
	return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query_capacity, struct block_header* heap_start) {
	if (query_capacity<=0) return NULL;
	if (!heap_start) return NULL;
    query_capacity = capacity_max(query_capacity);
    struct block_search_result result = try_memalloc_existing(query_capacity, heap_start);
    if (result.type == BSR_FOUND_GOOD_BLOCK) return result.block;
    if (result.type == BSR_CORRUPTED) return NULL;
    struct block_header *header = grow_heap(result.block, query_capacity);
    if (!header) return NULL;
    return try_memalloc_existing(query_capacity, header).block;
}

void* _malloc( size_t query_capacity ) {
  struct block_header* const addr = memalloc( query_capacity, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header))
	  ;
}
